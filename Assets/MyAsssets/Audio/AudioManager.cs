﻿using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    // Start is called before the first frame update
    void Start()
    {
        foreach( Sound n in sounds)
        {
            n.soundSource = gameObject.AddComponent<AudioSource>();
            n.soundSource.clip = n.soundClip;

            n.soundSource.volume = n.volume;
        }
    }
     
    
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null) { return; }
        s.soundSource.Play();
    }
}
