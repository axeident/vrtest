﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FiringRayCast : MonoBehaviour
{
    public GameObject player;
    float maxRange = 100f;
    public int time_to_select = 3000;
    private int  time_looked_at = 0;
    public Text textbox;

    private void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, maxRange) && hit.collider.gameObject.CompareTag("Select_Trigger"))
        {
            time_looked_at++;
            if (time_looked_at >= time_to_select)
            {
                FindObjectOfType<AudioManager>().Play("Transition");
                player.GetComponent<Scene_Updater>().scene_update(hit.collider.gameObject.GetComponent<Selection_Data>().stage, hit.collider.gameObject.GetComponent<Selection_Data>().key);
            }
        }
        else
        {
            time_looked_at = 0;
        }
        textbox.text = time_looked_at.ToString();
    }
    

}
