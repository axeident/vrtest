﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene_Updater : MonoBehaviour
{
    public Material light;
    public Material dark;
    public GameObject ground;
    public void scene_update(int stage, int key)
    {
        if (stage == 0 && key == 0)
        {
            ground.GetComponent<Renderer>().material = light;
        } 
        if (stage ==0 && key == 1)
        {
            ground.GetComponent<Renderer>().material = dark;
        }
    }
}
